package ru.t1.malyugin.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.dto.request.scheme.SchemeDropRequest;
import ru.t1.malyugin.tm.dto.request.scheme.SchemeUpdateRequest;
import ru.t1.malyugin.tm.dto.response.scheme.SchemeDropResponse;
import ru.t1.malyugin.tm.dto.response.scheme.SchemeUpdateResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ISchemeEndpoint extends IEndpoint {

    @NotNull
    String NAME = "SchemeEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static ISchemeEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ISchemeEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, ISchemeEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ISchemeEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, ISchemeEndpoint.class);
    }

    @NotNull
    @WebMethod
    SchemeDropResponse dropScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull SchemeDropRequest request
    );

    @NotNull
    @WebMethod
    SchemeUpdateResponse updateScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull SchemeUpdateRequest request
    );

}