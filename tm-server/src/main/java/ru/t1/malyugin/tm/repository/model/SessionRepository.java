package ru.t1.malyugin.tm.repository.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.malyugin.tm.model.Session;

@Repository
@Scope("prototype")
public interface SessionRepository extends AbstractUserOwnedRepository<Session> {

}