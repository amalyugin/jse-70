package ru.t1.malyugin.tm.migration;

import liquibase.exception.LiquibaseException;
import org.junit.Test;

public class UserSchemeTest extends AbstractSchemeTest {

    @Test
    public void userTest() throws LiquibaseException {
        LIQUIBASE.dropAll();
        LIQUIBASE.update("user");
    }

}