package ru.t1.malyugin.tm.migration;

import liquibase.exception.LiquibaseException;
import org.junit.Test;

public class SessionSchemeTest extends AbstractSchemeTest {

    @Test
    public void sessionTest() throws LiquibaseException {
        LIQUIBASE.dropAll();
        LIQUIBASE.update("session");
    }

}