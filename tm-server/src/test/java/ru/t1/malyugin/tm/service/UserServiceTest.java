package ru.t1.malyugin.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.malyugin.tm.api.service.dto.IProjectDTOService;
import ru.t1.malyugin.tm.api.service.dto.ITaskDTOService;
import ru.t1.malyugin.tm.api.service.dto.IUserDTOService;
import ru.t1.malyugin.tm.api.service.property.IPropertyService;
import ru.t1.malyugin.tm.configuration.ServerConfiguration;
import ru.t1.malyugin.tm.dto.model.UserDTO;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.exception.field.EmailEmptyException;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.LoginEmptyException;
import ru.t1.malyugin.tm.exception.field.PasswordEmptyException;
import ru.t1.malyugin.tm.exception.user.EmailExistException;
import ru.t1.malyugin.tm.exception.user.LoginExistException;
import ru.t1.malyugin.tm.exception.user.UserNotFoundException;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.util.HashUtil;

import static ru.t1.malyugin.tm.TestData.*;

@Category(UnitCategory.class)
public class UserServiceTest {
    private static IPropertyService PROPERTY_SERVICE;

    private static IProjectDTOService PROJECT_DTO_SERVICE;

    private static ITaskDTOService TASK_DTO_SERVICE;

    private static IUserDTOService USER_DTO_SERVICE;

    @BeforeClass
    public static void setUp() throws LiquibaseException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);

        PROJECT_DTO_SERVICE = context.getBean(IProjectDTOService.class);
        USER_DTO_SERVICE = context.getBean(IUserDTOService.class);
        PROPERTY_SERVICE = context.getBean(IPropertyService.class);
        TASK_DTO_SERVICE = context.getBean(ITaskDTOService.class);

        @NotNull final Liquibase liquibase = context.getBean(Liquibase.class);
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void initTest() {
        for (int i = 1; i <= NUMBER_OF_USERS; i++) {
            @NotNull final UserDTO user = new UserDTO();
            user.setLogin("TST_" + i);
            user.setPasswordHash("TST");
            user.setEmail("tst@mail.ru" + i);
            USER_DTO_SERVICE.add(user);
            USER_LIST.add(user);

            PROJECT_DTO_SERVICE.create(user.getId(), "NEW", "NEW");
            TASK_DTO_SERVICE.create(user.getId(), "NEW", "NEW");
        }
    }

    @After
    public void clearData() {
        TASK_DTO_SERVICE.clear();
        PROJECT_DTO_SERVICE.clear();
        for (@Nullable final UserDTO user : USER_LIST) USER_DTO_SERVICE.remove(user);
        TASK_LIST.clear();
        PROJECT_LIST.clear();
        USER_LIST.clear();
    }

    @Test
    public void testCreateUser() {
        final long initialNumberOfUsers = USER_DTO_SERVICE.getSize();
        final long expectedNumberOfUsers = initialNumberOfUsers + 4;
        @NotNull final UserDTO user1 = USER_DTO_SERVICE.create("NEW1", "PASS1", "MAIL1", null);
        @NotNull final UserDTO user2 = USER_DTO_SERVICE.create("NEW2", "PASS2", "MAIL2", Role.ADMIN);
        @NotNull final UserDTO user3 = USER_DTO_SERVICE.create("NEW3", "PASS3", null, null);
        @NotNull final UserDTO user4 = USER_DTO_SERVICE.create("NEW4", "PASS4", null, null);
        USER_LIST.add(user1);
        USER_LIST.add(user2);
        USER_LIST.add(user3);
        USER_LIST.add(user4);

        Assert.assertEquals(expectedNumberOfUsers, USER_DTO_SERVICE.getSize());
        Assert.assertEquals("NEW1", user1.getLogin());
        Assert.assertEquals("MAIL1", user1.getEmail());
        Assert.assertEquals(HashUtil.salt(PROPERTY_SERVICE, "PASS1"), user1.getPasswordHash());
        Assert.assertEquals(Role.USUAL, user1.getRole());
        Assert.assertEquals("NEW2", user2.getLogin());
        Assert.assertEquals("MAIL2", user2.getEmail());
        Assert.assertEquals(HashUtil.salt(PROPERTY_SERVICE, "PASS2"), user2.getPasswordHash());
        Assert.assertEquals(Role.ADMIN, user2.getRole());
    }

    @Test
    public void testCreateUserNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_DTO_SERVICE.create(null, UNKNOWN_STRING, UNKNOWN_STRING, null));
        Assert.assertThrows(LoginEmptyException.class, () -> USER_DTO_SERVICE.create(null, UNKNOWN_STRING, UNKNOWN_STRING, Role.USUAL));

        Assert.assertThrows(PasswordEmptyException.class, () -> USER_DTO_SERVICE.create(UNKNOWN_STRING, null, UNKNOWN_STRING, null));
        Assert.assertThrows(PasswordEmptyException.class, () -> USER_DTO_SERVICE.create(UNKNOWN_STRING, null, UNKNOWN_STRING, Role.USUAL));

        @NotNull final UserDTO user = USER_LIST.get(0);
        Assert.assertThrows(LoginExistException.class, () -> USER_DTO_SERVICE.create(user.getLogin(), UNKNOWN_STRING, UNKNOWN_STRING, null));
        Assert.assertThrows(LoginExistException.class, () -> USER_DTO_SERVICE.create(user.getLogin(), UNKNOWN_STRING, UNKNOWN_STRING, Role.USUAL));

        Assert.assertThrows(EmailExistException.class, () -> USER_DTO_SERVICE.create(UNKNOWN_STRING, UNKNOWN_ID, user.getEmail(), null));
        Assert.assertThrows(EmailExistException.class, () -> USER_DTO_SERVICE.create(UNKNOWN_STRING, UNKNOWN_ID, user.getEmail(), Role.USUAL));
    }

    @Test
    public void testLockUserByLogin() {
        for (@NotNull final UserDTO user : USER_LIST) {
            USER_DTO_SERVICE.lockUser(user.getLogin());
            @Nullable final UserDTO actualUser = USER_DTO_SERVICE.findOneById(user.getId());
            Assert.assertNotNull(actualUser);
            Assert.assertEquals(true, actualUser.getLocked());
        }
    }

    @Test
    public void testLockUserByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_DTO_SERVICE.lockUser(null));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_DTO_SERVICE.lockUser(UNKNOWN_STRING));
    }

    @Test
    public void testUnlockUserByLogin() {
        for (@NotNull final UserDTO user : USER_LIST) {
            USER_DTO_SERVICE.lockUser(user.getLogin());
            @Nullable UserDTO actualUser = USER_DTO_SERVICE.findOneById(user.getId());
            Assert.assertNotNull(actualUser);
            Assert.assertEquals(true, actualUser.getLocked());
            USER_DTO_SERVICE.unlockUser(user.getLogin());
            actualUser = USER_DTO_SERVICE.findOneById(user.getId());
            Assert.assertNotNull(actualUser);
            Assert.assertEquals(false, actualUser.getLocked());
        }
    }

    @Test
    public void testUnlockUserByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_DTO_SERVICE.unlockUser(null));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_DTO_SERVICE.unlockUser(UNKNOWN_STRING));
    }

    @Test
    public void testRemoveByEmailNegative() {
        Assert.assertThrows(EmailEmptyException.class, () -> USER_DTO_SERVICE.removeByEmail(null));
    }

    @Test
    public void testSetPassword() {
        @NotNull final UserDTO user = USER_LIST.get(0);
        @NotNull final String pass = "NEWP";
        @Nullable final String passHash = HashUtil.salt(PROPERTY_SERVICE, pass);
        USER_DTO_SERVICE.setPassword(user.getId(), pass);
        @Nullable final UserDTO actualUser = USER_DTO_SERVICE.findOneById(user.getId());
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(passHash, actualUser.getPasswordHash());
    }

    @Test
    public void testSetPasswordNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> USER_DTO_SERVICE.setPassword(null, UNKNOWN_STRING));
        Assert.assertThrows(PasswordEmptyException.class, () -> USER_DTO_SERVICE.setPassword(UNKNOWN_STRING, null));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_DTO_SERVICE.setPassword(UNKNOWN_STRING, UNKNOWN_STRING));
    }

    @Test
    public void testUpdateProfile() {
        for (@NotNull final UserDTO user : USER_LIST) {
            @NotNull final String test = "TEST";
            USER_DTO_SERVICE.update(user.getId(), null, null, test);
            USER_DTO_SERVICE.update(user.getId(), null, test, null);
            USER_DTO_SERVICE.update(user.getId(), test, null, null);
            @Nullable final UserDTO actualUser = USER_DTO_SERVICE.findOneById(user.getId());
            Assert.assertNotNull(actualUser);
            Assert.assertEquals(test, actualUser.getFirstName());
            Assert.assertEquals(test, actualUser.getMiddleName());
            Assert.assertEquals(test, actualUser.getLastName());
        }
    }

    @Test
    public void testUpdateProfileNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> USER_DTO_SERVICE.update(null, UNKNOWN_STRING, UNKNOWN_STRING, UNKNOWN_STRING));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_DTO_SERVICE.update(UNKNOWN_STRING, UNKNOWN_STRING, UNKNOWN_STRING, UNKNOWN_STRING));
    }

    @Test
    public void testFindOneByLogin() {
        for (@NotNull final UserDTO user : USER_LIST) {
            Assert.assertEquals(user.getId(), USER_DTO_SERVICE.findOneByLogin(user.getLogin()).getId());
        }
    }

    @Test
    public void testFindOneByEmail() {
        for (@NotNull final UserDTO user : USER_LIST) {
            Assert.assertEquals(user.getId(), USER_DTO_SERVICE.findOneByEmail(user.getEmail()).getId());
        }
    }

    @Test
    public void testIsLoginExist() {
        for (@NotNull final UserDTO user : USER_LIST) {
            Assert.assertEquals(true, USER_DTO_SERVICE.isLoginExist(user.getLogin()));
        }
    }

    @Test
    public void testIsEmailExist() {
        for (@NotNull final UserDTO user : USER_LIST) {
            Assert.assertEquals(true, USER_DTO_SERVICE.isEmailExist(user.getEmail()));
        }
    }

}