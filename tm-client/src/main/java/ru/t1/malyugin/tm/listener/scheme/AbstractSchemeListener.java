package ru.t1.malyugin.tm.listener.scheme;

import org.apache.commons.lang3.ArrayUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.malyugin.tm.api.endpoint.ISchemeEndpoint;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.listener.AbstractListener;

public abstract class AbstractSchemeListener extends AbstractListener {

    @NotNull
    @Autowired
    protected ISchemeEndpoint schemeEndpoint;

    @NotNull
    @Override
    public Role[] getRoles() {
        return ArrayUtils.toArray();
    }

}