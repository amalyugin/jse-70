package ru.t1.malyugin.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1.malyugin.tm.model.Task;

import java.util.Collection;

@RequestMapping("/api/task")
public interface ITaskRestEndpoint {

    @GetMapping("/get/{id}")
    Task getById(
            @PathVariable("id") final String id
    );

    @PostMapping("/post")
    void create(
            @RequestBody final Task task
    );

    @PutMapping("/put")
    void update(
            @RequestBody final Task task
    );

    @DeleteMapping("/delete/{id}")
    void deleteById(
            @PathVariable("id") final String id
    );

    @GetMapping("/get")
    Collection<Task> getList();

    @GetMapping("/count")
    long count();

    @DeleteMapping("/delete/all")
    void clearList();

}