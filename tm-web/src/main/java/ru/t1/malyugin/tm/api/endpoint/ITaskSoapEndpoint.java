package ru.t1.malyugin.tm.api.endpoint;

import ru.t1.malyugin.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface ITaskSoapEndpoint {

    @WebMethod
    Task getById(
            @WebParam(name = "id", partName = "id") String id
    );

    @WebMethod
    void create(
            @WebParam(name = "task", partName = "task") Task task
    );

    @WebMethod
    void update(
            @WebParam(name = "task", partName = "task") Task task
    );

    @WebMethod
    void deleteById(
            @WebParam(name = "id", partName = "id") String id
    );

    @WebMethod
    Collection<Task> getList();

    @WebMethod
    long count();

    @WebMethod
    void clearList();

}