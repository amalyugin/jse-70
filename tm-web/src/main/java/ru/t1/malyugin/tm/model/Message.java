package ru.t1.malyugin.tm.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Message {

    private String value;

    public Message(final String value) {
        this.value = value;
    }

}