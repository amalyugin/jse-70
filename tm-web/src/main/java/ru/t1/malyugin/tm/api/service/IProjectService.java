package ru.t1.malyugin.tm.api.service;

import ru.t1.malyugin.tm.model.Project;

import java.util.Collection;

public interface IProjectService {

    Collection<Project> findAll();

    Collection<Project> findAllForUser(String userId);

    long count();

    long countForUser(String userId);

    void create();

    void createForUser(String userId);

    void add(Project project);

    void addForUser(String userId, Project project);

    void deleteById(String id);

    void deleteByIdForUser(String userId, String id);

    void clear();

    void clearForUser(String userId);

    void edit(Project project);

    void editForUser(String userId, Project project);

    Project findById(String id);

    Project findByIdForUser(String userId, String id);

}