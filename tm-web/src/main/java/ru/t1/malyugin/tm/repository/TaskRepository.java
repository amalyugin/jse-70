package ru.t1.malyugin.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.malyugin.tm.model.Task;

import java.util.Collection;
import java.util.Optional;

public interface TaskRepository extends JpaRepository<Task, String> {

    Collection<Task> findAllByUserId(String userId);

    Optional<Task> findByUserIdAndId(String userId, String id);

    long countByUserId(String userId);

    boolean existsByUserIdAndId(String userId, String id);

    void deleteByUserIdAndId(String userId, String id);

    void deleteAllByUserId(String userId);

}